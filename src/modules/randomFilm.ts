import { MoviesCategory, Movie } from '../types';
export const randomFilm = (movie: Movie) => {
    const html = `<div class="row py-lg-5">
<div
    class="col-lg-6 col-md-8 mx-auto"
    style="background-image: url('https://image.tmdb.org/t/p/original/${movie.poster_path}')"
>
    <h1 id="random-movie-name" class="fw-light text-light">${movie.title}</h1>
    <p id="random-movie-description" class="lead text-white">
        ${movie.overview}
    </p>
</div>
</div>`;
    const film = document.createElement('div');
    film.innerHTML += html;
    return film;
};
