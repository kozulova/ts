import { favouriteFilm } from './favouriteMoviesTemplate';
import { Movie } from '../types';
import { getMovieById } from '../helpers/getMovies';

export const likeMovie = (e: Event) => {
    const likedMovie: HTMLElement = e.target as HTMLElement;
    toogleRedHeart(likedMovie);
    addLikedMovieToLocalStorage(likedMovie);
    addFovouriteMovies();
};

const toogleRedHeart = (movie: HTMLElement) => {
    movie.getAttribute('fill') === 'red' ?
     movie.setAttribute('fill', '#ff000078') : 
     movie.setAttribute('fill', 'red');
};

const addLikedMovieToLocalStorage = (movie: HTMLElement) => {
    if (localStorage.getItem('movies_id') === null) {
        const likedMovies: Array<String> = [movie.id];
        localStorage.setItem('movies_id', JSON.stringify(likedMovies));
    } else {
        let likedMovies = JSON.parse(
            localStorage.getItem('movies_id') as string
        );
        if (likedMovies.indexOf(movie.id.toString()) >= 0) {
            likedMovies = likedMovies.filter(
                (id: string) => id !== movie.id.toString()
            );
        } else {
            likedMovies.push(movie.id);
        }
        localStorage.setItem('movies_id', JSON.stringify(likedMovies));
    }
};

export const likeMovieFromLocalStorage = (): void => {
    if (localStorage.getItem('movies_id') !== null) {
        const likedMovies = JSON.parse(
            localStorage.getItem('movies_id') as string
        );
        likedMovies.forEach((id: string) => {
            let movieHeardElement: HTMLElement | null =
                document.getElementById(id);
            movieHeardElement && toogleRedHeart(movieHeardElement);
        });
    }
};

export const addFovouriteMovies = (): void => {
    const favoriteMoviesElement: HTMLElement | null =
        document.getElementById('favorite-movies') as HTMLElement;
        favoriteMoviesElement.textContent = '';
    if (localStorage.getItem('movies_id') !== null) {
        const likedMovies = JSON.parse(
            localStorage.getItem('movies_id') as string
        );
        likedMovies.forEach(async (id: string) => {
            let movie: Movie = await getMovieById(id);
            favoriteMoviesElement &&
                favoriteMoviesElement.appendChild(favouriteFilm(movie));
        });
    }
};
