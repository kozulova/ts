import { film } from './modules/filmTemplate';
import { randomFilm } from './modules/randomFilm';
import { getMovies } from './helpers/getMovies';
import { MoviesCategory, Movie } from './types';
import {
    likeMovie,
    likeMovieFromLocalStorage,
    addFovouriteMovies
} from './modules/likeMovie';

let state = {
    movies: [],
    category: '',
    page: 1
};

export async function render(): Promise<void> {
    renderTopMovies();
    renderRandomMovie();
    addListeners();
    addFovouriteMovies();
}

const addListeners = () => {
    const popularBtn = document.getElementById('popular');
    popularBtn?.addEventListener('click', renderPopularMovies);

    const upcomingBtn = document.getElementById('upcoming');
    upcomingBtn?.addEventListener('click', renderUpcomingMovie);

    const topBtn = document.getElementById('top_rated');
    topBtn?.addEventListener('click', renderTopMovies);

    const submitBtn = document.getElementById('submit');
    submitBtn?.addEventListener('click', renderFoundMovies);

    const loadMore = document.getElementById('load-more');
    loadMore?.addEventListener('click', loadMoreFilms);
    
    const navBarIcon = document.querySelector('.navbar-toggler');
    navBarIcon?.addEventListener('click', ()=>{
        renderLikes();      
    })
};

const renderMovies = (movies: Array<Movie>) => {
    const filmContainer: HTMLElement = document.getElementById(
        'film-container'
    ) as HTMLElement;
    filmContainer.textContent = '';
    movies.forEach((item) => filmContainer?.appendChild(film(item)));
    likeMovieFromLocalStorage();
    renderLikes();
};

const renderLikes = () => {
    const likeFilm = document.querySelectorAll('.bi');
    likeFilm?.forEach((el) => el.addEventListener('click', likeMovie, false));
}

const renderFoundMovies = async () => {
    const input: string = document.getElementById('search')?.value;
    if (input.length > 0) {
        const movies: Array<Movie> = await getMovies(
            undefined,
            undefined,
            input
        );
        renderMovies(movies);
    }
};

const renderTopMovies = async () => {
    const movies: Array<Movie> = await getMovies(MoviesCategory.top_rated);
    state.movies = movies as any;
    state.category = MoviesCategory.top_rated;
    state.page = 1;
    renderMovies(movies);
};

const renderRandomMovie = async () => {
    const randomMovie: HTMLElement | null =
    document.getElementById('random-movie');
    const movies: Array<Movie> = await getMovies(MoviesCategory.top_rated);
    randomMovie?.appendChild(
        randomFilm(movies[Math.floor(Math.random() * movies.length)])
    );
};

const renderPopularMovies = async () => {
    const movies: Array<Movie> = await getMovies(MoviesCategory.popular);
    state.movies = movies as any;
    state.category = MoviesCategory.popular;
    state.page = 1;
    renderMovies(movies);
};

const renderUpcomingMovie = async () => {
    const movies: Array<Movie> = await getMovies(MoviesCategory.upcoming);
    state.movies = movies as any;
    state.category = MoviesCategory.upcoming;
    state.page = 1;
    renderMovies(movies);
};

const loadMoreFilms = async () => {
    state.page++;
    const movies: Array<Movie> = await getMovies(state.category as MoviesCategory, state.page);
    state.movies = [...state.movies, ...movies] as any;
    renderMovies(state.movies);
}