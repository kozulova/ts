import { MoviesCategory, Movie } from '../types';
const ApiKey = '0515251fa31f55a923e750e79e1008e4';
const apiUrl = 'https://api.themoviedb.org/3/';

const getMovies = async (
    category?: MoviesCategory,
    page?: number,
    query?: string | undefined
): Promise<Array<Movie>> => {
    let apiLink: string = '';
    if (query != undefined) {
        apiLink = `${apiUrl}search/movie?api_key=${ApiKey}&query="${query}`;
    } else {
        apiLink = `${apiUrl}movie/${category}?api_key=${ApiKey}`;
    }
    if (page) {
        apiLink = apiLink + `&page=${page}`;
    }
    const request: Response = await fetch(apiLink);
    const movies: any = await request.json();
    return movies.results.map(mapper);
};

export const getMovieById = async (id: string): Promise<Movie> => {
    const apiLink = apiUrl + `movie/${id}?api_key=${ApiKey}`;
    const request: Response = await fetch(apiLink);
    const movie = await request.json();   
    return mapper(movie);
};

const mapper = (item: any): Movie => {
    const movie = {
        id: item.id,
        title: item.title,
        overview: item.overview,
        release_date: item.release_date,
        poster_path: item.poster_path,
    };
    return movie;
};

export { getMovies };
