export interface Movie {
    id: number,
    title: string,
    overview: string,
    release_date: string,
    poster_path: string
}

export enum MoviesCategory{
    top_rated = "top_rated",
    popular = "popular",
    upcoming = 'upcoming'
}